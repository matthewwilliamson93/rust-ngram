# fuzzy_search_rs

A simple PyO3 wrapper around rust crates like ngrammatic and fst to allow for a python interface for doing fuzzy searches efficiently.

## Development

| Instruction | Command  |
|---|---|
| Start by creating a venv | `make venv` |
| Activate that venv | `source .venv/bin/activate` |
| Install dependencies |`make install-dev-deps` |
| Build and install the package | `make install` |
| Run the tests | `make test` |

### All Other Commands

```
Usage:
    help:             Prints this screen
    venv:             Creates a virtual env
    install-dev-deps: Installs dev dependencies
    build:            Builds the Rust code into a pypi package
    install:          Installs the local pypi package into the env
    check-fmt:        Checks the code for style issues
    fmt:              Make the formatting changes directly to the project
    lint:             Lints the code
    test:             Runs the local tests
    clean:            Clean out temporaries
```

## Layout

```
.
├── Cargo.lock
├── Cargo.toml           - Main config file for building the rust locally
├── Makefile             - A proxy for commands to be run locally
├── pyproject.toml       - Main config file for building the pypi package
├── README.md
├── requirements-dev.txt - Dev dependencies needed for running tests
├── src
│   └── lib.rs           - Main implementation of the PyO3 wrapper logic
└── tests
    └── test_perf.py     - A pytest module designed to test the performance of the package.
```

## Performance

The following is output from `make test`

```
=============================================================================================== test session starts ===============================================================================================
platform linux -- Python 3.10.0, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
rootdir: /home/mwilli20/Documents/projects/rust-ngram
collected 2 items

tests/test_perf.py ..                                                                                                                                                                                       [100%]

===================================================================================================== PASSES ======================================================================================================
________________________________________________________________________________________________ test_perf_create _________________________________________________________________________________________________
---------------------------------------------------------------------------------------------- Captured stdout call -----------------------------------------------------------------------------------------------
......Benchmarking Create
+---------------+---------+---------+---------+
| Functions     |   Small |  Medium |   Large |
+---------------+---------+---------+---------+
| rust_create   | 0.00017 | 0.00192 | 0.03381 |
| python_create | 0.00292 | 0.00886 | 0.05841 |
+---------------+---------+---------+---------+
________________________________________________________________________________________________ test_perf_search _________________________________________________________________________________________________
---------------------------------------------------------------------------------------------- Captured stdout call -----------------------------------------------------------------------------------------------
......Benchmarking Search
+---------------+---------+---------+---------+
| Functions     |   Small |  Medium |   Large |
+---------------+---------+---------+---------+
| rust_search   | 0.00903 | 0.12271 | 1.49052 |
| python_search | 0.01133 | 0.10263 | 0.84643 |
+---------------+---------+---------+---------+
================================================================================================ 2 passed in 3.06s ================================================================================================
```
